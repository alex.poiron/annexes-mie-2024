# annexes-mie-2024

- **combinations_metrics_table.pdf**: A table listing all the metrics combinations retrieved in the selected studies. These combinations are organized according to the machine learning context (binary classification, multi classification or regression).
- **criteria_identification.pdf**: Figure showing how we identified and defined our criteria evaluation.
- **references.pdf**: All the studies from the scoping review.
- **selection_process.pdf**: Figure showing how we selected our studies for the scoping review.
- **studies_analyses.pdf**: Analyses of the selected studies showing the different types of data encountered, the clinical application of the CDSS presented and the different AI-based models used.